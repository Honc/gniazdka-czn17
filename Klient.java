import java.net.*;
import java.io.*;
import javax.swing.*;
import java.awt.event.*;

public class Klient extends JPanel
        implements Runnable,ActionListener {
    private int port;
    private Socket s;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private String host,m="",m1="";
    JTextField nazwa = new JTextField(20);
    JTextArea komentarz = new JTextArea(4,18);
    
    
    Klient(String host_, int port_) {
        host = host_;
        port = port_;
        setSize(300,160);
        nazwa.addActionListener(this);  
        JLabel etykieta_nazwy = new JLabel("Napisz");
        JLabel etykieta_komentarza = new JLabel  ("Rozmowa");
        komentarz.setLineWrap(true);
        komentarz.setWrapStyleWord(true);
        add(etykieta_nazwy);
        add(nazwa);
        add(etykieta_komentarza);
        add(komentarz);
        JScrollPane obszar_przewijany1 = new JScrollPane
                (komentarz,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        add(obszar_przewijany1); 
    }
    public void actionPerformed(ActionEvent evt) {
        Object zrodlo = evt.getSource();
        if(zrodlo==nazwa) {
            m1=nazwa.getText();
            if (s!=null)
                try {
                    output.writeObject(m1);
                } catch(Exception e) {
                    System.out.println("Wyjatek klienta "+e);}
        }
        repaint();
    }
    
    public void run() {
        String pom;
        try {
            s = new Socket(host,port);
            input = new ObjectInputStream(s.getInputStream());
            output = new ObjectOutputStream(s.getOutputStream());
            output.flush();
            komentarz.setText("Klient startuje na hoscie "+
                    InetAddress.getLocalHost().getHostName()+"\n");
            }catch (Exception e) {
            System.out.println("Wyjatek klienta "+e);}

        try {
            do
            { if(!m1.equals("czesc")&&!m.equals("czesc")) {
                  m=(String) input.readObject();
                  pom=komentarz.getText();
                  komentarz.setText(pom+"Dane odebrane od serwera: "+m+"\n");
              }
            }while(!m.equals("czesc"));
            s.close();
            s=null;
            output.close();
            input.close();
           }catch (Exception e) {
            System.out.println("Wyjatek klienta "+e);}
        
    }
    
    public static void main(String args[]) throws Exception {
        String s = InetAddress.getLocalHost().getHostName();
        Klient k2= new Klient(s,6666);
        Thread t = new Thread(k2);
        t.start();
        JFrame ramka = new JFrame("Klient");
        ramka.setSize(300, 160);
        ramka.setContentPane(k2);
        ramka.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ramka.setVisible(true);
    }
}