import java.net.*;
import java.io.*;
import javax.swing.*;
import java.awt.event.*;


class Server_component extends JFrame implements Runnable,
        ActionListener 
{
    private Socket s;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private String m="",m1="";
    JTextField nazwa = new JTextField(20);
    JTextArea komentarz = new JTextArea(4,18);
    
    public Server_component(Socket s_,ObjectInputStream input_, ObjectOutputStream output_) 
    {
        super("Serwer"+(++Server.nr));
        s=s_;
        input=input_;
        output=output_;
        setSize(300,160);
        nazwa.addActionListener(this);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JPanel panel=new JPanel();
        JLabel etykieta_nazwy = new JLabel("Napisz");
        JLabel etykieta_komentarza = new JLabel  ("Rozmowa");
        komentarz.setLineWrap(true);
        komentarz.setWrapStyleWord(true);
        panel.add(etykieta_nazwy);
        panel.add(nazwa);
        panel.add(etykieta_komentarza);
        panel.add(komentarz);
        JScrollPane obszar_przewijany1 = new JScrollPane
                (komentarz,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        panel.add(obszar_przewijany1);
        setContentPane(panel);
        setVisible(true);
       }
    
    public void actionPerformed( ActionEvent evt) {
        Object zrodlo = evt.getSource();
        if(zrodlo==nazwa) {
            m1=nazwa.getText();
            if (!m1.equals("czesc")&& s!=null)
                try {
                    output.writeObject(m1);
                } catch(Exception e) {
                    System.out.println("Wyjatek serwera "+e);}
        }
        repaint();
    }
    
    public void run() {
        String pom;
        try {
            komentarz.setText("Serwer startuje na hoscie "+
                    InetAddress.getLocalHost().getHostName()+"\n");
            while(true) 
            {
                m=(String) input.readObject();
                pom=komentarz.getText();
                komentarz.setText(pom+"Odebrano wiadomosc od klienta: "+ m +"\n");
                if (m.equals("czesc")) 
                {
                    m1="czesc";
                    output.writeObject(  m1);
                    break;
                }
            }
            input.close();
            output.close();
            s.close();
            s=null;
           }catch (Exception e) {
            System.out.println("Wyjatek serwera "+e);}
    }
}

public class Server implements Runnable {
    private int sPort;
    private ServerSocket serwer;
    private Socket s;
    private ObjectOutputStream output;
    private ObjectInputStream input;
    private String host;
    static int nr=0;
    
    public Server(int port_, String host_) {
        sPort = port_;
        host=host_;
        try {
            serwer = new ServerSocket(sPort);
        }catch(IOException e) {
            System.out.println(e);}
    }
    
    
    public void run() {
        System.out.println("Serwer startuje na hoscie "+host);
        while (true) {
            try {
                s = serwer.accept();
               }catch (IOException e) {
                System.out.println("Nie mozna polaczyc sie z klientem "+e);
                System.exit(1);}
            if (s!=null) {
                try {
                    output = new ObjectOutputStream(s.getOutputStream());
                    output.flush();
                    input = new ObjectInputStream(s.getInputStream());
                    Thread t=new Thread(new Server_component(s, input, output));
                    t.start();
                }catch (Exception e) {
                    System.out.println("Wyjatek serwera "+e);}
            }
        }
    }
    
    public static void main(String args[]) throws Exception {
        String host_ = InetAddress.getLocalHost().getHostName();
        int Port = 6666;
        Server s2 = new Server(Port, host_);
        Thread t = new Thread(s2);
        t.start();
    }
    
}